<?php


namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Article;
use Doctrine\ORM\EntityRepository;
use Faker\Factory;

/**
 * Class InMemoryArticleRepository
 *
 * @package AppBundle\Entity\Repository
 */
class InMemoryArticleRepository implements ArticleRepository
{
	/**
     * @return mixed
     */
    public function findAll()
    {
    	$faker = Factory::create();

    	dump($faker->name);
        $articles = array();

        foreach (range(1, 10) as $articleNumber) {
            $articles[] = new Article(
                $articleNumber,
                sprintf(
                    'article_%s',
                    $articleNumber
                ),
                $faker->paragraph,
                'https://picsum.photos/500/500'
            );
        }

        return $articles;
    }
}