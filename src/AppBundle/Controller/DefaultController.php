<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Repository\InMemoryArticleRepository;

/**
 * Class DefaultController
 *
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
    	$articles = $this->getDoctrine()->getRepository('AppBundle:Article');

        return $this->render('default/index.html.twig', [
            'articles' 	=> $articles->findAll(),
            'count'		=> count($articles->findAll())
        ]);
    }

    /**
     * @Route("/second-section")
     * @param Request $request
     *
     * @return Response
     */
    public function secondSectionAction(Request $request)
    {
    	return $this->render('default/second.html.twig');
    }

    /**
     * @Route("/ajax-load")
     * @param Request $request
     *
     * @return Response
     */
    public function ajaxLoadAction(Request $request)
    {
    	$articles = new InMemoryArticleRepository;

        return new Response($this->renderView('default/content.html.twig', [
            'articles' 	=> $articles->findAll(),
            'count'		=> count($articles->findAll())
        ]), 200);
    }
}
